require 'time'
require 'pry'

class SessionMapper
  attr_reader :old_times, :new_times

  def initialize(old_times, new_times)
    @old_times = old_times
    @new_times = new_times
  end

  class << self
    def call(old_times, new_times)
      new(old_times, new_times).call
    end
  end

  def call
    remove_suspended_old_times!

    ranked_mappings = ranked_times_between_sessions

    chosen_mappings = {}
    while !ranked_mappings.empty?
      mapping = ranked_mappings.first
      old_time_idx = mapping[0]
      new_time_idx = mapping[1]
      chosen_mappings[old_time_idx] = new_time_idx
      ranked_mappings.reject! { |i, j, _| i == old_time_idx || j == new_time_idx }
    end

    result = {}
    chosen_mappings.each do |old_time_idx, new_time_idx, _|
      old_time = old_times[old_time_idx]
      new_time = new_times[new_time_idx]
      result[old_time] = new_time
    end
    result
  end

  private

  def remove_suspended_old_times!
    @old_times.reject! { |h| h[:state] == "suspended" }
  end

  def ranked_times_between_sessions
    result = []
    old_times.each_with_index do |old_time, i|
      new_times.each_with_index do |new_time, j|
        result << [i, j, seconds_between(old_time, new_time)]
      end
    end
    result.sort_by(&:last)
  end

  def seconds_between(old_time, new_time)
    [
      seconds_new_time_starts_before_old_time(old_time, new_time),
      seconds_new_time_starts_after_old_time(old_time, new_time)
     ].max
  end

  def seconds_new_time_starts_before_old_time(old_time, new_time)
    [Time.parse(old_time[:starts_at]) - Time.parse(new_time[:starts_at]), 0].max
  end

  def seconds_new_time_starts_after_old_time(old_time, new_time)
    [Time.parse(new_time[:ends_at]) - Time.parse(old_time[:ends_at]), 0].max
  end
end
