require_relative '../test_helper'
require_relative '../fixtures/sessions'

class SessionMapperTest < Minitest::Test
  include Fixtures::Sessions

  def setup
    @old_times = dummy_old_times
    @new_times = dummy_new_times
  end

  def test_call
    result = SessionMapper.call(@old_times, @new_times)
    assert result.is_a?(Hash)
  end

  def test_result_size
    result = SessionMapper.call(@old_times, @new_times)
    assert_equal(6, result.size)
  end
end

