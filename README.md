# Session Mapper

## Description

This project contains a solution for a challenge to create mapping logic that takes a collection of existing session data and new session data, and maps the existing sessions to the new sessions whilst following certain criteria.

## Session Mapping Criteria

### Input

`SessionMapper` takes two arguments:
1. `old_times`: an array of hashes defining the start and end times of existing sessions (in ISO8601 format), as well as a state attribute with one of the following values: `available`, `booked` or `suspended`
2. `new_times`: an array of hashes defining the start and end times of the new sessions (in ISO8601 format)

Each `old_times` argument will always have:
* exactly 8 sessions
* exactly 2 suspended sessions (so that there are at most 6 mappable sessions)

Each `new_times` argument will always have:
* exactly 6 sessions

Other criteria to note:
* The first session in `old_times` and `new_times` will always start at the same time.

### Output

`SessionMapper` returns a hash that maps the sessions from `old_times` to the sessions from `new_times`. The old hash should be used as the key, and the new hash should be used as the value.

# Prerequisites

* `ruby` - version 3.0.0

# Running the tests

This project uses Minitest for testing.

Below are examples of ways to run the tests from the Unix command prompt. Each example has been written relative to the project's root directory.

To run all tests:
```
ruby test/all.rb
```

To run all tests for a single test file (for example):
```
ruby test/lib/session_mapper_test.rb
```

To run a specific test from a specific file (for example):
```
ruby test/lib/session_mapper_test.rb --name=test_call
```

## Design choices

The main design choice I made was deciding to use my own algorithm for the mapping:
* I calculated the "time differences" between each of the old and new times and used those differences to sort/rank the potential old-to-new pairings.
* I then selected the first pairing for each old/new time that hadn't been used yet.

My reasoning for this was simply that I wanted to ship a solution during the allocated time frame, rather than spending too long researching sorting algorithms and not shipping anything.

Other design choices:
* I structured the class similarly to how a Service object might be structured in Rails. This allowed to me to extract logic into private helper methods, to try to keep it simple.

## How do I feel the project went?

I was very excited when I saw the challenge initially, as it is a real-world problem unlike anything I have done before.

My attempt didn't go as well as I had hoped, but I do believe this solution strongly reflects my current ability.

## What did I learn from this project?

* To be more prepared when working on a device / development environment that you haven't used in a long time.
* That there is massive opportunity for me to learn and develop my experience with back-end algorithms.

## What would I do differently if I attempted this project again?

There are so many improvements that can be made to my current solution, including the following:
* **Better testing** (possibly RSpec), including a lot more test cases.
* **Better structuring of test objects** (possibly via fixtures or factories).
* **A more effective algorithm** - I can see at least 1 test case already that my solution wouldn't be ideal for: suppose the first 5 `old_times` were mapped to the last 5 `new_times`, which were only slightly better for them than the first 5 `new_times` - this would mean that the last `old_time` would end up being mapped to the first `new_time`, which could potentially be a significant inconvenience if that time had been booked.
* **Prioritising booked times vs available times** - my current solution treats both equally, meaning that an available time could be prioritised over a booked time, which could result in significant inconvenience for that time.
* I might have **reached out to the challenge designers to ask for clarification** on a couple of the details, to get a better feel for the challenge expectations.
* **More refactoring** - there is still a lot of logic happening in `SessionMapper#call` that could be extracted to private helper methods.
* **Research more effective sorting/ranking algorithms/solutions for this scenario** that deal with the prioritising of booked vs available times better to reduce the overall potential inconvenience.
* **General code tidyup** (e.g. `pry` should not be required in the main class file, that was only intended for testing).
* **Reviewed business requirements with the relevant people**, as there may be more context to this problem - e.g. how much flexibility around the mappings is there? Is anything else being done outside of this remapping method to help mitigate impact to users, e.g. sending emails/notifications to the users about this?

I expect that I would discover many more opportunities for improvement whilst working through this list.

